# Sicherheitsaspekte im Zusammenhang mit Kubernetes

[TOC]

## Überblick
Innerhalb von Kubernetes laufen Pods die wiederum Container beherbergen. Die Sicherheitsaspekte, auf die bei Kubernetes geachtet werden muss, sind deshalb mit denen bei Containern sehr gut zu vergleichen. Die Problemzonen die bei Containern finden Sie in der Theorie [Sicherheitsaspekte im Zusammenhang mit Containern](../../Container/Sicherheitsaspekte/Readme.md). Bei Kubernetes werden aber folgende Sicherheitsbereiche noch zusätzlich verschärft:
- **Fehler in der Konfiguration**: Kubernetes ist um ein vielfaches komplexer zu konfigurieren. Entsprechend ist hier die Gefahr grösser, dass Fehlkonfigurationen passieren und dadurch Sicherheitslücken entstehen.
- **Kontrolle des Netzwerk-Verkehrs**: Dadurch, dass Kubernetes ein Cluster ist, der aus verschiedenen Nodes besteht, die typischerweise öffentlich zugängliche IP-Adressen besitzen, wird es umso wichtiger die Kommunikation zwischen den Containern zu verschlüsseln, weil dieser Netzwerk-Verkehr bei einer Man-in-the-middle-Attacke sonst einfacher abgefangen werden könnte, wenn dieser unverschlüsselt wäre.

Der Aspekt bezüglich fehlender Sicherheitsupdates wird allerdings etwas entspannter. Kubernetes bietet die Möglichkeit an, ein rollendes Update einzuspielen. Dadurch können Sicherheitsupdates im laufenden Betrieb eingespielt werden, ohne dass die Container bzw. der Dienst für eine kurze Zeit ausfällt. Die Kunden können auch während der Installation von Updates weiterhin bedient werden in dem ein Replica nach dem anderen Ausgetauscht wird und so immer noch einige Kopien der Pods bestehen bleiben, die die Anfragen der Benutzer beantworten können.

## Massnahmen
Da die Massnahmen ebenfalls sehr ähnlich sind, konzentrieren wir uns in dieser Theorie nur auf die zusätzlichen Massnahmen, die es nur für Kubernetes gibt. Die Massnahmen für Container entnehmen Sie bitte der Theorie [Sicherheitsaspekte im Zusammenhang mit Containern](../../Container/Sicherheitsaspekte/Readme.md).

### Automatisierung Update-Prozesse in Kubernetes
Beim Einspielen von Updates auf Kubernetes im laufenden Betrieb wird von [Rolling Updates](https://kubernetes.io/docs/tutorials/kubernetes-basics/update/update-intro/) gesprochen. Um den Update-Prozess der Pods in Kubernetes zu automatisieren, wird in der Regel mit den Tags (Versionsnummern) bei Containern gearbeitet. Wenn neue Tags / Commits erkannt werden, werden diese durch die [CI/CD-Pipeline](https://ubuntu.com/blog/kubernetes-ci-cd-pipelines-what-why-and-how) automatisiert getestet und deployed.

### Überwachung und Sicherung des Netzwerkverkehrs zwischen Pods mit istio (envoy-proxy) und kiali
Mit istio existiert ein OpenSource-Tool zur Überwachung und Sicherung des Netzwerk-Verkehrs zwischen Pods. Neben dem istio control plane wird in jedem Pod zusätzlich zu den Containern des Pods auch via Container injection ein envoy proxy Container injected. Dieser übernimmt die Kontrolle des Netzwerk-Verkehrs, der in den Pod rein oder aus dem Pod raus geht:

![istio service mesh](../../Ressourcen/Images/service-mesh-istio.svg)

Quelle & Details: https://istio.io/latest/about/service-mesh/

Mit Kiali existiert ein Webinterface, dass auf istio aufsetzt und die durch istio zur Verfügung gestellten Daten übersichtlich darstellt. Hier ein Beispiel einer - aufgrund der Daten - generierten Netzwerktopologie:

![Kiali Netzwerk Topologie Feature](../../Ressourcen/Images/kiali-topology-graph-node-animation.gif)

Quelle & Details: https://kiali.io/docs/ & https://kiali.io/docs/features/topology/ (Info zu Topology-Feature)

Wenn Sie istio und kiali gerne auf dem LernMAAS ausprobieren möchten, finden Sie unter https://istio.io/latest/docs/setup/additional-setup/getting-started/ eine Anleitung, wie Sie istio und kiali auf Kubernetes installieren können.