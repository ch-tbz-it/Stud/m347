# Kubernetes erklärt

[TOC]

## Basics

*Kubernetes* oder kurz *K8s* ist der de-facto Standard für den Betrieb moderner Microservice Applikationen. Es ist, ähnlich zu *docker compose*, ein System zur Container-Orchestrierung. Anders als *docker compose* ermöglicht *Kubernetes* den Betrieb von Containern auf mehreren Host-Systemen (*Nodes*).

Kubernetes wurde ursprünglich von Google in Go entwickelt und ist seit 2014 als Open Source Projekt für eine breite Masse verfügbar.

[Kubernetes Video](https://www.youtube.com/watch?v=VnvRFRk_51k&list=PLy7NrYWoggjziYQIDorlXjTvvwweTYoNC&index=1)

## Warum Kubernetes?

Während sich Microservice-Applikationen immer mehr verbreitet haben, sind mehrere Bedürfnisse entstanden, die vorher noch nicht durch ein einheitliches System gelöst werden konnten.

- **Verfügbarkeit** - Nutzer erwarten heute eine hohe Verfügbarkeit von Web-Anwendungen. Ausgefallene Microservices müssen ohne spürbaren Zeitverlust ersetzt werden können. Mit Kubernetes werden ausgefallene Ressourcen automatisch ersetzt und Services werden entsprechend neu auf die Ressourcen verteilt. 
- **Skalierbarkeit** - Der Aufwand für die Skalierung einer Microservice Applikation soll möglichst gering sein. Insbesondere agile Entwicklungsmethoden und DevOps Prinzipien erfordern eine einfache Erhöhung der Nutzerbasis während des Produktlebenszyklus. Kubernetes bietet eine einfache Skalierung über die Konfiguration der einzelnen Services.
- **Wartbarkeit** - Fehlerbehebungen und Weiterentwicklungen einzelner Services müssen im Sinne von *Continous Integration* und *Continuous Delivery* (CI/CD) einfach und schnell auf der Infrastruktur ausgerollt werden können. Anhand von Definitionen in yml-Dateien können in Kubernetes viele Aufgaben automatisiert werden.

## Architektur und Begriffe

![](../Ressourcen/Images/kubernetes_architecture.png)

Ein Kubernetes Cluster besteht in der Regel aus mindestens einer *Control Plane* (*master node*) und mehreren Arbeitsmaschinen (den *worker nodes*). 

Es gibt mehrere Möglichkeiten, auf das Kubernetes Control Plane zuzugreifen, je nachdem, wie Sie Ihre Kubernetes-Cluster konfiguriert haben und welche Art von Zugriff Sie benötigen. In der obigen Abbildung sind drei Varianten abgebildet: 
- UI (kubernetes dashboard)
- API (Aufrufe über eine Programmierschnittstelle)
- CLI (command line interface, z.B. mit kubectl)

### Control Plane
In der Control Plane werden alle Aufgaben der Container Orchestrierung durchgeführt.

#### API Server
Der zentrale Kommunikationsknoten des Clusters.
Alle Interaktionen mit dem Cluster werden über Kubernetes-API-Aufrufe ausgeführt. Kubernetes-API-Calls können direkt über HTTPs oder indirekt über Befehle im  Kubernetes-Command-Line-Client (**kubectl**) sowie das Kubernetes-UI (**Dashboard**) ausgeführt werden.

#### Controller Manager
Der Controller Manager ist eine der Kernkomponenten der Control Plane. Seine Hauptaufgabe besteht darin, sicherzustellen, dass der Clusterzustand mit den deklarativen Konfigurationszielen übereinstimmt. Dies wird erreicht, indem verschiedene Controller im Controller Manager betrieben werden, um verschiedene Aspekte des Clusters zu überwachen und zu steuern.

Hier sind einige der wichtigsten Controller, die im Controller Manager laufen:
- Replication Controller/Replica Set Controller
- Deployment Controller
- StatefulSet Controller
- DaemonSet Controller
- Job Controller
- CronJob Controller
- Service Account & Token Controller
- Namespace Controller
- Endpoint Controller

#### Etcd
Etcd ist das Persistenz-Backend und enthält Informationen zur Cluster-Konfiguration, also welche Nodes, Ressourcen innerhalb des Clusters verfügbar sind. Auch der Cluster-Zustand wird im Etcd gespeichert.

#### Kube Scheduler
Weist neu erstellte Pods einem verfügbaren Node zu.

### Nodes

Nodes sind entweder physische Rechner (z.B.: auch ein Raspberry Pi) oder eine VM. Damit ein Node an einem Kubernetes Cluster teilnehmen kann, müssen einige Dienste aktiv sein.

#### Kubelet
Ein Prozess, der auf jedem Knoten im Cluster ausgeführt wird. Ein Kubelet startet die Pods mit der verfügbaren Container-Engine (Docker, rkt usw.) und überprüft / meldet anhand spezifischer PodSpecs regelmäßig den Pod-Status.

#### Kube Proxy
Kube-Proxy überprüft Aufrechterhaltung der Netzwerkregeln auf dem Node und führt Verbindungsweiterleitungen an Service-Endpunkten aus.

#### Container-Runtime
Die Laufzeitumgebung für die Container. Kubernetes unterstützt mehrere Container-Engines: Docker, containerd, cri-o, rktlet und jede Implementierung des Kubernetes **CRI** (Container Runtime Interface).

#### Pod
Ein Pod besteht zumindest aus einem, meist aber einer Gruppe von Containern und ist zugleich die kleinste, bereitstellbare Einheit in Kubernetes. Innerhalb des Pod können die Container über localhost miteinander kommunizieren. Die Container teilen sich die Ressourcen des Pods (Volumes, Ip-Adresse, Ports).


---
**Quellen:**

- [kubernetes.io](https://kubernetes.io/docs/home/)
- [kubernetes deployment](https://kubernetes.io/de/docs/tutorials/kubernetes-basics/deploy-app/deploy-intro/)
- [bnerd](https://bnerd.com/de/knowledge/articles/1-kubernetes-fur-einsteiger)
- [Video Erklärung](https://www.youtube.com/watch?v=VnvRFRk_51k&list=PLy7NrYWoggjziYQIDorlXjTvvwweTYoNC&index=2)
