# deployment & statefulset

[TOC]

## deployment

Um eine hohe Verfügbarkeit zu erreichen, betreibt man die [Pod's](../node%20and%20pod/Readme.md) redundant. In Kubernetes nennt man sie *Replicas*. Sobald eine Pod nicht mehr verfügbar ist, wird der [Service](../service%20and%20ingress/Readme.md) auf die andere redundante Pod umgeleitet. Diese Aufgabe übernimmt die Komponente *Deployment*. Anstatt jedesmal eine Pod zu erstellen, wird mit Deplyoment ein sogenannter *Blueprint* (Schema) definiert. Somit ist ein Deployment eine Abstraktion von Pod.

![](../../../Ressourcen/Images/deployment.png)

## statefulset

Wie siehts nun bei einer Datenbank aus? Kann man diese auch mit einem Deployment hoch verfügbar machen? Die Antwort: *NEIN*. Herausfordernd ist, dass man z.B. für redundante Datenbanken nur eine zentrale Datenverwaltung hat und somit Dateninkonsistenzen unbedingt zu verhindern sind. Kubernetes hat dafür eine Lösung: *Statefulset*. Replicas können erstellt werden, aber diese Komponente stellt zusätzlich sicher, dass der Datenverkehr koordiniert erfolgt und somit daraus ein Mechanismus entsteht, welche Pod gerade Daten schreiben oder lesen darf.

**HINWEIS**: Arbeiten mit statefulset kann je nach Anforderungen zu einer höheren Komplexität führen. Deshalb kommt in der Praxis häufig vor, dass man Datenbanken nicht unbedingt in einem Cluster aufbaut sondern komplett auserhalb betreibt.

![](../../../Ressourcen/Images/statefulset%20datainconsistences.png)

## Zusammenfassung

Zusammenfassend kann man sagen, dass [Deployments](#deployment) für *stateless* APP's angedacht sind. Bei *stateful* APP's oder Datenbanken verwendet man [Statefulset](#statefulset)

![](../../../Ressourcen/Images/statfulset_deplyoment.png)

## Quellen
- https://spacelift.io/blog/statefulset-vs-deployment
- https://www.youtube.com/watch?v=pPQKAR1pA9U