# volume

[TOC]

Pod's sind [*ephemeral*](../node%20and%20pod/Readme.md). Wenn Sie nun eine Datenbank und ihre Daten in einer Pod haben und diese z.B. neu gestartet wird, dann sind die Daten verloren. Um dies zu verhindern, müssen Sie die Daten auslagern (ausserhalb des Clusters), z.B. in einem Fileserver (on-premis) oder in einem Cloud-Speicher (cloud-storage). Kubernetes bietet keine Speicher-Komponenten an. Hierbei muss der Kubernetes Administrator/ User selbst um die Datenverwaltung und die Konfiguration kümmern.

## Quellen
- [Volume](../../../Container/docker/Volumes.md)

![](../../../Ressourcen/Images/volume.png)

