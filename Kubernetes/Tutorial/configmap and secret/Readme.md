# configmap

[TOC]

Unsere Webapplikation my-app hat eine Anbindung zu einer Datenbank. In der Regel ist der Datenbank-Endpoint in der Applikation konfiguriert. Falls sich nun der Endpoint ändern würde, müsste man die Applikation neu builden, eine neue Image erstellen, in eine Image Repository pushen und anschliessend zum Pod wiederum pullen. Um diesen Aufwand zu vereinfachen, hat Kubernetes die Komponente **configmap** geschaffen. 

Configmap ist eigentlich eine key/value store. In unserem Beispiel wird nun der Datenbank-Endpoint unter dem Namen *DB_URL* mit dem Value *mongo-db* gespeichert. Die APP holt sich die DB-URL bzw. Datenbank-Endpoint aus dem Configmap unter dem Key *DB_URL*.

![](../../../Ressourcen/Images/configmap.png)



# secret

Kubernetes stellt noch eine weitere key/value store zu Verfügung. Im Gegensatz zu configmap, in welcher nicht-vertrauliche Daten gespeichert werden sollten, speichert man in secret schützenswerte und vertrauliche Daten. In unserem Beispiel *mongo-user* und *mongo-pwd*.

>>In Kubernetes werden Secrets nicht automatisch verschlüsselt. Stattdessen werden sie base64-codiert, um sie vor direktem Lesen zu schützen. Base64-Codierung ist jedoch keine Form der Verschlüsselung. Es ist lediglich eine einfache Codierungsmethode, die dazu verwendet wird, Daten zu obfuskieren, sodass sie nicht im Klartext sichtbar sind. Dies bietet jedoch keine Sicherheit auf hohem Niveau, da Base64 leicht dekodiert werden kann, wenn jemand Zugriff auf das Secret erhält.

Um Secrets sicher nutzen zu können, befolgen Sie mindestens die folgenden Schritte:
1. Verwenden Sie Verschlüsselung: Sie können Verschlüsselung auf Anwendungsebene verwenden, bevor Sie die sensiblen Informationen in einem Secret speichern. Wenn die Daten verschlüsselt sind, selbst wenn jemand Zugriff auf das Secret erhält, sind die Daten für sie nutzlos, es sei denn, sie verfügen über den erforderlichen Entschlüsselungsschlüssel.

2. Verwenden Sie Kubernetes RBAC: Stellen Sie sicher, dass Sie Kubernetes Role-Based Access Control (RBAC) verwenden, um sicherzustellen, dass nur autorisierte Benutzer oder Dienste auf Secrets zugreifen können.

3. Verwenden Sie Tools für Secret-Management: Es gibt Tools von Drittanbietern, die speziell für das sichere Management von Secrets in Kubernetes entwickelt wurden. Beispiele sind HashiCorp Vault, Sealed Secrets und andere. Diese Tools bieten zusätzliche Sicherheitsschichten und Funktionen für das sichere Speichern und Abrufen von Secrets.

4. Begrenzen Sie die Zugriffe: Erteilen Sie nur den Diensten und Anwendungen Zugriff auf Secrets, die ihn tatsächlich benötigen. Vermeiden Sie es, Secrets an Pods oder Anwendungen weiterzugeben, die keinen Zugriff darauf benötigen.

Beischreibung zu Secret: https://kubernetes.io/docs/concepts/configuration/secret/

Best Practice zu Secret: https://kubernetes.io/docs/concepts/security/secrets-good-practices/

![](../../../Ressourcen/Images/secret.png)

# Zusammenfassung

Sowohl [configmap](#configmap) als auch [secret](#secret) werden als Objekte in einem verteilten Speicher in [/etcd](https://etcd.io/docs/v3.5/learning/why/) gespeichert.