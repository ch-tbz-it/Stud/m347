# Tutorial

[TOC]

In diesem Dokument erklären wir Ihnen Schritt für Schritt, wie ein Kubernetes Cluster aufgebaut und konfiguriert wird. Hierbei verwenden wir einen einfachen use-case: Eine einfache Webapplikation betrieben mit einer einfachen Datenbank.

Wir erklären Ihnen die wichtigsten Kubernetes Komponenten, welche der Kubernetes Administrator oder User verwendet:
- Pod
- Service
- Ingress
- ConfigMap
- Secret
- Deployment
- StatefulSet
- DeamonSet

## Wichtigsten K8s Komponenten

### [Node & Pod](node%20and%20pod/Readme.md)

### [Service & Ingress](service%20and%20ingress/Readme.md)

### [ConfigMap & Secret](configmap%20and%20secret/Readme.md)

### [Volume](volume/Readme.md)

### [Deployment & StatefulSet](deployment%20and%20statefulset/Readme.md)

## Grundsätzliches zu Kubernetes Konfiguration mit YAML

### [Kubernetes Konfiguration mit YAML-Files](./Kubernetes%20Configuration%20with%20yaml/Readme.md)







