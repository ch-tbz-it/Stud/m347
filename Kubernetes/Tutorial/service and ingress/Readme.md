# service

[TOC]

In Kubernetes haben die Pods oft dynamische IP-Adressen (z.B. Pod muss neu erstellt werden nach einem crash), während Kubernetes Services in der Regel eine virtuelle IP-Adresse haben, die als ClusterIP bezeichnet wird. Diese virtuelle IP-Adresse ist statisch und stellt einen stabilen Endpunkt für die Kommunikation mit den Pods dar.

Ein Kubernetes-Service ist eine Abstraktion, die eine Gruppe von Pods repräsentiert und ihnen eine stabile, virtuelle IP-Adresse (ClusterIP) zuweist. Diese ClusterIP-Adresse ist statisch und ändert sich nicht, solange der Service besteht. Sie stellt eine feste Schnittstelle für die Kommunikation mit den Pods dar, selbst wenn die Pods selbst neu gestartet oder verschoben werden. Der Service leitet Anfragen an die zugehörigen Pods weiter, unabhängig von deren IP-Adressen.

## Zusammenfassung
Würde man nun die Pod's und ihre Adresse direkt verwenden, müsste man bei einer Pod Wiederherstellung jedesmal die neue dynamische IP-Adresse neu konfigurieren. Um dies zu verhindern verwendet man Services. In diesem Beispiel haben my-app Pod und DB Pod jewils einen eigenen Service. Die Lifecycle von Pod & Service sind getrennt, d.h. sollte ein Pod crashen, der Service und die dazugehörige ClusterIP bleiben bestehen und der Pod wird mit einer neuen eigenen IP-Adresse neu erstellt und mit dem Service wieder verbunden.

![](../../../Ressourcen/Images/kubernetesservice.png)


# ingress
In Kubernetes gibt es verschiedene Arten von Services, die in erster Linie darauf abzielen, die Erreichbarkeit von Pods und Anwendungen innerhalb und außerhalb des Clusters zu steuern. Dazu gehören "Internal Services" (interner Dienst) und "External Services" (externer Dienst):

1. **Internal Service (Cluster-internal Service)**: Ein interner Service ist ein Kubernetes-Service, der dazu dient, auf Pods und Anwendungen innerhalb des Kubernetes-Clusters zuzugreifen. Dies bedeutet, dass der Service nur innerhalb des Clusters über seine ClusterIP-Adresse erreichbar ist. Dies ist nützlich, wenn Sie eine interne Kommunikation zwischen verschiedenen Teilen Ihrer Anwendung benötigen, die in verschiedenen Pods ausgeführt werden.

2. **External Service (Cluster-external Service)**: Ein externer Service ist ein Kubernetes-Service, der dazu dient, auf Pods und Anwendungen von außerhalb des Kubernetes-Clusters zuzugreifen. Dies ermöglicht es, Anfragen von außerhalb des Clusters, beispielsweise aus dem Internet oder anderen Netzwerken, an Ihre Anwendungen weiterzuleiten. Es gibt verschiedene Arten von externen Services, darunter NodePort und LoadBalancer, die unterschiedliche Methoden für die Erreichbarkeit von außerhalb des Clusters bieten. 

In Kubernetes muss man beim Erstellen eines Services auch den **Servicetyp** angeben. Bei nicht-Deklarierung ist per default der internal Service konfiguriert.

Möchte man nun den external Service ausserhalb des Clusters ansprechen, müsste man im Browser die IP-Adresse der Node und die Portnummer des Services angeben, z.B. http://124.36.5.3:8080 
Für Testzwecke ist dies sicher nützlich, jedoch für den produktiven Einsatz unbrauchbar. Viel lieber möchte man eine FQDN (fully qualified domain name) haben. In unserem Beispiel für die my-app: https://my-app.com 

![](../../../Ressourcen/Images/ingress.png)

**Hierzu kommt nun [ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/) ins Spiel.**

![](../../../Ressourcen/Images/ingressfinal.png)

Die [Ingress-Ressource](https://kubernetes.io/docs/concepts/services-networking/ingress/) ist eine Kubernetes-Ressource, die die Konfiguration für die Verwaltung des Datenverkehrs definiert. Sie legt Regeln fest, die bestimmen, wie der eingehende HTTP- oder HTTPS-Datenverkehr geroutet und an die richtigen Services in Ihrem Cluster weitergeleitet wird.

---
Quellen: 
https://kubernetes.io/docs/concepts/services-networking/ingress/

