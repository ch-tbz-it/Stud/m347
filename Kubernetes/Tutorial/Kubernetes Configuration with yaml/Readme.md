# Kubernetes Konfiguration mit YAML

Wie bereits in der [Kubernetes Architektur](../../Readme.md#architektur-und-begriffe) beschrieben, gibt es verschiedene Schnittstellen (UI, API oder CLI), die Konfigurationen eines Kubernetes Clusters zu laden. In der Regel erfolgt das Hochladen eines YAML oder JSON files via [kubectl CLI](https://kubernetes.io/docs/reference/kubectl/).

![](../../../Ressourcen/Images/kubernetes%20Interfaces.png)

## 3 Bestandteile eines YAML-Files
Beim nachfolgenden Beispiel erklären wir Ihnen anhand zwei YAML-Files vom Typ [*deployment*](../deployment%20and%20statefulset/Readme.md) und [*service*](../service%20and%20ingress/Readme.md) die drei Bestandteile.

1. <span style="color:red">API Version/ kind</span>: Deklariert einerseits die API-Version vom Control-Pane und andererseit wird der Typ des YAML-Files angegeben. In diesem Beispiel *deployment* und *service*.
2. <span style="color:yellow">Metadata</span>: Name definieren, label für Referenzierung
3. <span style="color:green">Specification</span>: Spezifizierung des Typs. Bei Deployment werden mit Replicas die Anzahl Pod's definiert.

[Status](https://kubernetes.io/docs/concepts/overview/working-with-objects/#object-spec-and-status): Ist im YAML file nicht sichtbar. Wird automatisch generiert und hinzugefügt von Kubernetes. Dieser wird im [*etcd*](../../Readme.md#etcd) im Hintergrund verwaltet. 



![](../../../Ressourcen/Images/deployment_vs_service.png)


---
Quellen: 
- kubectl: https://kubernetes.io/docs/reference/kubectl/
- kubernetes status: https://kubernetes.io/docs/concepts/overview/working-with-objects/#object-spec-and-status
- kubernetes fields: https://kubernetes.io/docs/concepts/overview/working-with-objects/#required-fields
- https://gitlab.com/ch-tbz-it/Stud/m347/-/blob/pat/Exkurse/YAML.md