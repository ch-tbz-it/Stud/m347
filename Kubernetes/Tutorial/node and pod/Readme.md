# node & pod

[TOC]

Ein Node ist eine physische oder virtuelle Maschine, auf der Container ausgeführt werden können. Ein Node kann als Host für Container betrachtet werden. Kubernetes verwaltet eine Gruppe von Nodes, die als Cluster bezeichnet werden. Jeder Node kann eine oder mehrere Pods hosten. Die Nodes sind für die Bereitstellung von Ressourcen wie CPU, Speicher und Netzwerkverbindung verantwortlich. Sie bilden die Grundlage für die verteilte Infrastruktur von Kubernetes.

Ein Pod ist die **kleinste berechenbare Einheit** in Kubernetes. Ein Pod ist eine Gruppe von einem oder mehreren Containern, die auf demselben Node ausgeführt werden und den gleichen Netzwerknamenraum, den gleichen Speicher und die gleiche IP-Adresse teilen. Pods werden oft als "Wrapper" für Container angesehen. Ein Pod kann eine einzelne Containeranwendung oder eine Gruppe von Containern umfassen, die eng miteinander zusammenarbeiten. Sie teilen denselben Lebenszyklus und können direkt miteinander kommunizieren, da sie denselben Netzwerkraum und dieselbe IP-Adresse haben.

Die Verwendung von Pods ermöglicht es Kubernetes, Container leichter zu verwalten und zu planen. In vielen Fällen werden Anwendungen in einzelnen Pods bereitgestellt, aber es gibt auch Szenarien, in denen mehrere Pods in einer Anwendung zusammenarbeiten, z. B. wenn sie Daten gemeinsam nutzen oder als Frontend- und Backend-Komponenten dienen.

In Kubernetes bezieht sich *Pods ephemeral* auf die Tatsache, dass Pods in Kubernetes in der Regel als kurzlebige, temporäre Einheiten betrachtet werden. Dies bedeutet, dass Pods in der Regel nicht dauerhaft sind und von Kubernetes je nach Bedarf erstellt, gestoppt, neu gestartet oder verschoben werden können.

## Zusammenfassung
Nodes sind die Infrastruktur, auf der Pods ausgeführt werden, und Pods sind die kleinste ausführbare Einheit in Kubernetes, die Container oder Containergruppen enthält.

![](../../../Ressourcen/Images/nodesandpods.png)

