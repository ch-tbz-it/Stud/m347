# microk8s

[TOC]

microK8s ist eine Vereinfachung von Kubernetes. microk8s kann - wie minikube - als single node agieren, aber auch - wie Kubernetes - mehrere Server zu Cluster zusammenschliessen. 

## Installation

Folgende Tools werden installiert:

- `microk8s`: Mit diesem Tool können Sie die nodes administrieren. 
- `kubectl`:  Mit diesem Tool können Sie den Cluster dann verwalten. Da dieses Tool in microK8s eingebettet ist, müssen Sie es wie folgt aufrufen: `microk8s kubectl`. Zur Vereinfachung haben Sie folgende Möglichkeiten:
  - Sie installieren kubectl zusätzlich unabhängig. 
  - Sie erstellen ein Alias mit dem Befehl `alias kubectl='microk8s kubectl'`. Sie können dies auch dem Cloud-init hinzufügen, wenn Sie möchten.



### Server erstellen

Für die Installation der Server wurde ein [Cloud-Init für MicroK8s](microk8s.yaml)  bereitgestellt. Sie können dies einfach direkt so verwenden, aber **fügen Sie unbedingt Ihren eigenen SSH-Key hinzu, ohne den bestehenden zu entfernen**.

Wichtig ist zudem, dass das OS, Netzwerk und Sicherheitsgruppen korrekt eingestellt sind. Stellen Sie folgende Schritte sicher:

- Jede Instanz hat eine statische private IP und eine statische öffentliche IP
- Die Sicherheitsgruppe/Firewall blockiert keine Kommunikation zwischen den Nodes und dem Master. Natürlich müsste man hier einschränken, aber wir wollen momentan keine Probleme im Netzwerk schaffen. 
- OS ist Ubuntu mit mindestens 2 Cores, 4GB Ram und 30GB Disk.

Detaillierte Anleitungen finden Sie unter den folgenden Links:

- [AWS](Installation_aws.md)
- LernMaas: TBD



### Cluster bilden

Folgen Sie den wenigen Schritten der [Anleitung von MicroK8s](https://microk8s.io/docs/clustering) und fügen Sie die drei Instanzen zu einem Cluster zusammen. 



## Quellen

- [MicroK8s: Getting Started](https://microk8s.io/docs/getting-started)
- [MicroK8s: Create a Cluster](https://microk8s.io/docs/clustering)