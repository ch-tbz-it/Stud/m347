# Installation AWS

[TOC]

Im Folgenden wird mit Screenshots gezeigt, welche Objekte angelegt werden sollten. Die Details, wie Sie die Objekte anlegen, haben Sie im Modul m346 bereits geübt. 

## Subnetz

Überlassen Sie die Auswahl des Subnetzes nicht dem Zufall, wählen Sie eines spezifisch aus. Hier wurde es auch benannt, so dass man es einfacher auswählen kann. Sie werden später den IP Range benötigen, Sie können Ihn sich hier bereits notieren. Im vorliegenden Fall also 172.31.64.0/20.

![subnetz](x_res/subnetz.png)

## Sicherheitsgruppe

Erstellen Sie nun die Sicherheitsgruppe. Stellen Sie sicher, dass Sie **nur innerhalb des Subnetzes** den gesamten Datenverkehr erlauben. Der SSH Port soll natürlich auch von aussen erreichbar sein. 

![sicherheitsgruppe](x_res/sicherheitsgruppe.png)

## Netzwerkinterface

Es gibt verschiedene Möglichkeiten wie Sie IPs den Instanzen zuweisen. Ich habe die Netzwerk-Interfaces vordefiniert. Erstellen Sie zuerst öffentliche IPs, dann erstellen Sie drei Netzwerkschnittstellen mit eigenen definierten IPs und weisen dann noch die öffentlichen IPs zu. Die hier ausgewählten privaten IPs sind, 172.31.64.100, 172.31.64.110, 172.31.64.120. Sie müssen natürlich im ausgewählten Subnetz liegen!

![oeffentlicheIPs](x_res/oeffentlicheIPs.png)

![Netzwerkschnittstellen](x_res/Netzwerkschnittstellen.png)

## Instanz

Erstellen Sie nun die drei Instanzen gemäss Angaben aus Readme.md. In AWS können Sie den Typ *t2.medium* verwenden. 

Die meisten Einstellungen sollten klar sein. Wichtig ist, dass Sie nun Ihr Netzwerkinterface auswählen. Gehen Sie dazu wie folgt vor.

![instanz1](x_res/instanz1.png)

![instanz1](x_res/instanz2.png)





