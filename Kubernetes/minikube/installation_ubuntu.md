# Installation minikube auf Windows

Sie können die Cloud-Init Datei verwenden, welche Ihnen minikube installiert (z.B. unter AWS oder Azure)

 [Cloud-Init Datei](minikube-ubunut.yaml) 

Stellen Sie sicher, dass Ihre VM Ubuntu installiert und mindestens 2 Cores, 2GB Ram und 30 GB Speicher hat (z.B. Typ t2.medium in AWS). 

Installiert wird dabei minikube, kubectl und docker, da in dieser Konfiguration minikube Docker als Virtualisierung verwendet. Zusätzlich wird nginx als Reverse Proxy installiert, so dass die Cluster Webseite erreichbar ist.

**Wichtig**: Sie müssen minikube korrekt ausführen und den Docker-Driver angeben. minikube wird nicht automatisch gestartet. Der Befehl lautet:

```bash
# startet minikube und verwendet docker als Virtualisierung!
minikube start --driver docker
```

**Einschränkung:**

- Das Dashboard funktioniert nicht. Es wäre noch zusätzlicher manueller Aufwand notwendig. Wahrscheinlich reicht ein zusätzlicher Reverse-Proxy

**Quellen:**

- https://minikube.sigs.k8s.io/docs/start/
- https://minikube.sigs.k8s.io/docs/drivers/
- https://minikube.sigs.k8s.io/docs/handbook/dashboard/