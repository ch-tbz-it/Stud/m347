# minikube

[TOC]

Minikube ist ein Open-Source-Tool zum Betreiben von Kubernetes-Clustern auf lokalen Entwicklungsmaschinen. Es ermöglicht Entwicklern und DevOps-Teams, Kubernetes-Cluster auf ihren eigenen Laptops oder Desktop-Computern zu erstellen, um Anwendungen lokal zu entwickeln, zu testen und bereitzustellen, bevor sie auf produktiven Umgebungen ausgeführt werden.

Minikube führt einen Kubernetes-Cluster mit einem *einzigen Node* in einer VM auf Ihrem Laptop aus. Typischerweise wird er als Befehlszeilen-Tool verwendet und ermöglicht es Benutzern, ohne dass eine vollständige Kubernetes-Umgebung aufgebaut werden muss. Minikube unterstützt mehrere Hypervisoren wie VirtualBox, Hyper-V und KVM oder auch Docker selbst, was es zu einer flexiblen Option für Entwickler auf verschiedenen Betriebssystemen macht.

[Minikube Video](https://www.youtube.com/watch?v=E2pP1MOfo3g)

![](../../Ressourcen/Images/virtualbox%20minikube.jpg)

## Installation minikube
[Installation auf Windows](installation_windows.md)

[Installation auf Ubuntu Server (Cloud)](installation_ubuntu.md)

## Auftrag 1: minikube commandline
- [minikube commandline](../00%20Aufträge/01%20minikube%20commandline/Readme.md)
## Auftrag 2: minikube yaml
- [minikube yaml](../00%20Aufträge/02%20minikube%20yaml/Readme.md)

---
Quellen:
- https://minikube.sigs.k8s.io/docs/



