# Installation minikube auf 

## Hypervisor
>**Hinweis:** Minikube unterstützt auch die Option `--vm-driver=none`, mit der die Kubernetes-Komponenten auf dem Host und nicht in einer VM ausgeführt werden. Die Verwendung dieses Treibers erfordert Docker und eine Linux-Umgebung, jedoch keinen Hypervisor.

Da Sie bereits Docker installiert haben, müssen Sie jetzt nichts unternehmen.

## minikube installieren
Minikube sind für verschiedene OS verfügbar: [minikube istall](https://kubernetes.io/de/docs/tasks/tools/install-minikube/)

1. File herunterladen, z.B. Windows: [minikube-windows-amd64.exe](https://github.com/kubernetes/minikube/releases/latest)
2. File umbenennen nach minikube.exe und irgendwo ablegen
3. Environmentvariable unter path den Ablageort hinzufügen
4. Befehl zum starten von minikube ausführen:
    ```bash
    minikube start
    ```
5. Überprüfen Sie, ob der cluster mit dem Befehl läuft:
   ```bash
   minikube kubectl cluster-info
   ```

### kubectl installieren
Commandline tool, um verschiedene Anweisungen innerhalb eines Clusters ausführen zu können. Wird auch bei k8s installiert!

**Achtung** <br>
*kubectl* muss nicht unbedingt installiert werden. Ist bereits in minikube vorhanden.
```bash
minikube kubectl -- version
```

Falls Sie jedoch nicht jedesmal *minikube kubectl* eingeben möchten, können Sie ein *alias* festlegen:

```bash
#oder Abkürzung in bash shell config
alias kubectl="minikube kubectl --"
#Test, ob alias funktioniert
kubectl version
```

## minikube dashboard
Minikube stellt ein Dashboard zur Verfügung. Mit dieser UI werden Kubernetes Komponenten visualisiert dargestellt. Geben Sie folgenden Befehl ein:

```bash
minikube dashboard
```



---
Quellen:<br>
- https://minikube.sigs.k8s.io/docs/start/
- https://minikube.sigs.k8s.io/docs/drivers/
- https://minikube.sigs.k8s.io/docs/handbook/dashboard/