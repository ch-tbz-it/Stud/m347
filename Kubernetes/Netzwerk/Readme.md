# Kubernetes Netzwerk

[TOC]

## Überblick
Kubernetes ist - wie bei der [Einführung ins Thema](../Readme.md) gelernt - ein Clustersystem, das auf hohe Verfügbarkeit der Applikation ausgelegt ist. Hochverfügbarkeit geht leider nicht ohne ein gewisses Mass an Komplexität. Das ist der Grund wieso im Zusammenhang mit Kubernetes die Netzwerkkonfiguration sehr komplex und vielschichtig ist.

Kubernetes führt im Vergleich zu Docker weitere Ebenen der Abstraktion ein. Bei Docker erhält jeder Container seine eigene IP-Adresse. Bei Kubernetes erhält jeder Pod seine eigene IP-Adresse. Die Container innerhalb eines Pods teilen sich dabei die IP-Adresse. Dass bedeutet, dass sich die beiden Container über localhost:port (beispielsweise localhost:80) ansprechen können. Es bedeutet aber zeitgleich auch, dass zwei Container mit gleichem Port nicht in einem Pod deployed werden können. Deshalb werden / sollten Sie in der Regel nur einen Container pro Pod haben. Sinnvolle Ausnahmen von dieser Regel sind können Sie bei Interesse unter https://kubernetes.io/blog/2015/06/the-distributed-system-toolkit-patterns/ nachlesen.

Das darüber liegende Netzwerkkonstrukt ist bei Kubernetes eher komplex und lässt sich auf vielfältige Weise individuell gestalten. [Calico](https://www.tigera.io/project-calico/) wird normalerweise bei microk8s eingesetzt. Die Dokumentation von Calico in Bezug auf Kubernetes finden Sie unter https://docs.tigera.io/calico/latest/getting-started/kubernetes/. Es wäre aber auch vorstellbar, dass anstelle von Calico ein anderer Manager für das Netzwerk eingesetzt würde. Es kann also sein, dass Sie in der Praxis im Arbeitsalltag im Netzwerkbereich anders funktionierenden Kubernetes-Installationen begegnen. Für den Moment konzentrieren wir uns auf Calico und dessen funktionsweise. Diese ist unter https://www.youtube.com/watch?v=NFApeJRXos4 sehr gut erklärt.

>**Hinweis**:  
> Die Netzwerk-Themen sind vor allem wegen des Clusterings (verteilen des Traffics auf die verschiedenen Nodes im Cluster) sehr komplex. Es muss sichergestellt werden, dass ein Client, der bereits eine Verbindung zum Cluster aufgebaut hat, auch von dem System eine Antwort erhält, welches er angefragt hat. Ansonsten wird der Client - wenn er von einem anderen Absender eine Antwort erhält wie er es erwartet - die Antwort des Systems verwerfen. Es braucht viel zusätzliche Logik, um Themen wie diese zu lösen, was das ganze Netzwerksetup verkompliziert. 
> 
> Es würde den Rahmen dieses Moduls sprengen die volle Komplexität des Netzwerks bei Kubernetes anzuschauen. Deshalb fokussieren wir uns auf den Teilaspekt wie der Zugriff über einen Port des Nodes - in unserem Fall Ihre VM auf dem LernMAAS - runter bis auf den konkreten Pod via NodePorts funktioniert.

## Beispiel IP's microk8s (LernMAAS)
Gegeben ist ein Container mit einem NGINX-Webserver, der auf der IP einer VM im LernMAAS exposed werden soll, sowie ein MySQL-Container, der nur durch den NGINX-Container erreicht werden soll. Folgendes Bild zeigt beispielhaft zwei mögliche IP-Konfigurationen auf einer VM im LernMAAS:

```plantuml
skinparam monochrome true
skinparam packageStyle rectangle
hide circle
hide empty members


package "LernMAAS Netzwerk (10.3.32.0/24)"{
    
    package "Client (10.3.32.71)" as C{
    } 
    note "Die VPN-IP Ihres Rechners" as Note1
    Note1 .. C

    package "Node 1 (10.3.32.10) - Port 31012" as N1 {
        package "my-nginx-svc Service (10.152.183.198) - Port 1234" as SVC1 {
            class "Pod1 (10.1.65.81) - Port 80" as POD1 {
                nginx (Port 80)
            }
            class "Pod2 (10.1.65.82) - Port 3306" as POD2 {
                mysql (Port 3306)
            }
            class "Pod3 (10.1.65.83) - Port 80 und 3306" as POD3 {
                nginx (Port 80)
                mysql (Port 3306)
            }
        }
    }
    
    note "Die IP-Adresse Ihrer VM, \nPort muss in Range von 30000 bis 32767 \nliegen bei default-Einstellungen" as Note2
    Note2 .. N1
    
    note "Die IP-Adresse des Services \n(wird beim erstellen des Services generiert), \nPort frei wählbar" as Note3
    Note3 .. SVC1
    
    note "Pod IP-Adressen werden beim Start \nder Pods durch Kubernetes automatisch vergeben, \nPort abhängig von Container-Inhalt bzw. Diensten, \ndie in Containern laufen und deren Port-Konfiguration" as Note4
    Note4 .. POD1
    Note4 .. POD2
    Note4 .. POD3

    C - N1
}

```

Das Beispiel besteht aus drei Pods und einem Service vom Type **NodePort**. Der wichtigste Teil der Service-Definition ist, die Property **ports** die unterhalb von **spec** liegt. Hier ein Auszug aus dem YAML für die Service-Definition ("..." kennzeichnet weg gelassene Teile):
```yaml
apiVersion: v1
kind: Service
metadata:
  ...
spec:
  ...
  ports:
  - name: http
    nodePort: 31012
    port: 1234
    protocol: TCP
    targetPort: 80
...
```

Wenn so eine Situation vorliegt, dann wird der Port des Kubernetes-Nodes auf die IP des Services gemappt. Vom Service wird weiter auf den Pod gemappt. Der Pod leitet den Traffic dann zum Container weiter bzw. vom Container wieder zurück über Pod, Service und Node zu Ihrem Client.

Von den unterschiedlichen Positionen innerhalb des Netzwerks haben Sie unterschiedliche Zugriffsmöglichkeiten:
- **Zugriff innerhalb der Pod-Grenzen**: Beispielsweise wenn beim Pod3 der nginx-Container auf den mysql-Container zugreifen will, dann geschieht das über localhost:3306 (oder 127.0.0.1:3306) vom nginx-Container aus.
- **Zugriff auf anderen Pod innerhalb des gleichen Nodes**: Will der nginx-Container aus Pod1 auf den mysql-Container aus Pod2 zugreifen geschieht dies über 10.1.65.82:3306.
- **Zugriff von Node auf den Pod**: Wenn Sie sich in der Konsole vom Node 1 befinden (Ihrer LernMAAS-VM), können Sie beispielsweise auf den nginx-Container im Pod1 via 10.1.65.81:80 zugreifen. Wollen Sie lieber auf den nginx-Container im Pod3, wäre es via 10.1.65.83:80.
- **Zugriff vom Node auf den Service**: Beim Zugriff über den Service (und nicht über einen spezifischen Pod) müssen Sie von der Konsole vom Node1 aus via 10.152.183.198:1234 zugreifen. Dieser Zugriff sucht sich ein Pod, der den Bedingungen des Service-Selektors genügt und leitet den Traffic dort hin weiter. In der Service-Definition wird mit `targetPort: 80` angegeben, dass der Traffic von Port 1234 (definiert in `port: 1234` in der Service-Definition) an den Port 80 weiter geleitet (zu der Pod-Grenze... und der Pod weiss dann, dass es auf Port 80 des nginx-Containers muss).
- **Zugriff von Client auf den Service**: Beim Zugriff vom Client (Ihrem Rechner) auf den Node & Service ist die Zeile `nodePort: 31012` die entscheidende, die den Port auf dem Node festlegt. Von Ihrem Rechner aus geschieht der Zugriff via 10.3.32.10:31012. Von da wird der Traffic an 10.152.183.198:1234 geleitet und von da weiter zu einem der dafür infrage kommenden Pods auf Port 80 und von der Pod-Grenze weiter zum richtigen Container und den ganzen Weg wieder zurück.

>**Hinweis**:
> 
> Der Default-NodePort-Range ist von 30000 bis 32767 (https://kubernetes.io/docs/concepts/services-networking/service/#type-nodeport). Ohne die Standard-Einstellungen von Kubernetes zu verändern, können Sie NodePorts nur innerhalb dieses Bereiches vergeben.

