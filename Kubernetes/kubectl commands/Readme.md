# kubectl commands

[TOC]

Hier finden Sie wichtige commands für ihren K8s-Cluster.

## Basis Informationen der k8s-Komponenten

```shell
kubectl get node
kubectl get pod
kubectl get svc
kubectl get all
```

## get: Erweiterte Informationen der Komponenten
```shell
kubectl get pod -o wide
kubectl get node -o wide
```

## get: Detaillierte Informationen über spezifische Komponenten
```shell
kubectl describe svc {svc-name}
kubectl describe pod {pod-name}
```


## Log-Daten der Applikationen
```shell
kubectl logs {pod-name}
```

---
Quellen: 

- https://kubernetes.io/docs/reference/kubectl
- https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands