# Docker einfach erklärt

[TOC]

## Was sind docker container?
Docker Container sind eine Art light-Version von virtuellen Maschinen. 

Während virtuelle Maschinen die Hardware eines Rechners, also den Prozessor, die Festplatte, usw. virtualisieren (Hypervisor), virtualisieren Docker Container das Betriebssystem. Dies wird auch Containervirtualisierung genannt. Aber was bedeutet das?

Containervirtualisierung bedeutet, dass basierend auf einem auf der Maschine laufenden Host-Betriebssystems (z.B. Ubuntu) weitere Linux Distributionen wie CentOS, Debian oder auch Ubuntu, parallel betrieben werden können. Der Trick: es wird der Kern des Host-Betriebssystems (der Kernel) mit den Containern geteilt.

![docker_vs_vm](./../../Ressourcen/Images/docker_vs_vm.webp)

Dabei kann das Host-Betriebssystem den Zugriff auf Dateien, Hardware, Speicher und Geräte beliebig einschränken. Womit die einzelnen Container voneinander isoliert laufen können.

[Video Erklärung](https://www.youtube.com/watch?v=5GanJdbHlAA&list=PLy7NrYWoggjwPggqtFsI_zMAwvG0SqYCb&index=6)

## Vorteile eines Docker Containers?
Der wesentliche Vorteil von Containern ist es, dass diese sowohl deutlich weniger Speicher benötigen, als auch deutlich schneller laufen wie VMs. Man muss für das Hochfahren eines neuen Containers nicht ein komplettes VM-Image mit mehreren GB laden, sondern kann mit abgespeckten Linux Distributionen wie Alpine Linux bereits mit wenigen MB starten.

Container können sowohl lokal auf einem Laptop mit der Anwendung Docker gemanaged werden, als auch in der Cloud betrieben werden.

Doch müssen Sie nicht von 0 weg starten. Es gibt ein globales Verzeichnis namens Docker Hub (Repository) wo bereits fertige Container-Images mit den unterschiedlichsten Applikationen wie Webservern, Datenbanken, usw. bereits verfügbar sind und mit einem Klick bei Ihnen gestartet werden können.

Docker Container stellen somit eine schnelle und kostengünstige Variante dar, die unterschiedlichsten Web- und Offline-Anwendungen in Bereichen wie der Logistik zu betreiben. Diese können innerhalb von Sekunden gestartet, gestoppt und auch wieder gelöscht werden.


## Was sind Images?
Der Begriff „Image“ ist Ihnen im Zusammenhang mit Virtualisierung möglicherweise von virtuellen Maschinen (VMs) bekannt. Für gewöhnlich handelt es sich bei einem VM-Image um eine Kopie eines Betriebssystems. Ggf. enthält ein VM-Image weitere installierte Komponenten wie Datenbank und Webserver. Der Begriff entstammt einer Zeit, in der Software auf optischen Datenträgern wie CD-ROMs und DVDs verteilt wurde. Wollte man eine lokale Kopie des Datenträgers anlegen, erstellte man mit einer speziellen Software ein Abbild, auf Englisch „Image“.

Bei der Container-Virtualisierung handelt es sich um die konsequente Weiterentwicklung der VM-Virtualisierung. Anstatt einen virtuellen Computer (Maschine) mit eigenem Betriebssystem zu virtualisieren, umfasst ein Docker-Image in der Regel lediglich eine Anwendung. Dabei kann es sich um eine einzelne Binärdatei handeln oder um einen Verbund mehrerer Software-Komponenten.

Um die Anwendung auszuführen, wird aus dem Image zunächst ein Container erzeugt. Alle auf einem Docker-Host laufenden Container greifen auf denselben Betriebssystem-Kernel zurück. Dadurch sind Docker-Container und Docker-Images in der Regel deutlich leichtgewichtiger als vergleichbare virtuelle Maschinen und deren Images.

Die Konzepte Docker-Container und Docker-Image sind eng miteinander verknüpft. So kann nicht nur ein Docker-Container aus einem Docker-Image erzeugt werden, sondern auch aus einem laufenden Container ein neues Image.

## Weitere Begriffe aus der container Technologie

### docker deamon
Der Docker Deamon ist ein dauerhafter Hintergrundprozess, der Docker-Images, Container, Netzwerke und Speichervolumen verwaltet. Er „schaut“ ständig auf Docker-API-Anforderungen und verarbeitet sie.

### docker client
Der Docker-Client ermöglicht den Benutzern die Interaktion mit Docker. Er kann sich auf demselben Host wie der Daemon befinden oder eine Verbindung zu einem Daemon auf einem entfernten Host herstellen und mit mehr als einem Daemon kommunizieren. Der Docker-Client bietet eine Befehlszeilenschnittstelle (CLI = Command Line Interpreter), über die Sie einem Docker-Daemon Befehle zum Erstellen, Ausführen und Anhalten von Anwendungen erteilen können.

Der Hauptzweck des Docker-Clients besteht darin, ein Mittel zur Verfügung zu stellen, mit dem Images aus einer Registry gezogen und auf einem Docker-Host ausgeführt werden können. Befehle werden unter [docker command line interface](CommandLineInterface.md) erklärt.

### Docker Registry

In Docker Registries werden Images versioniert abgelegt und verteilt.

Die Standard-Registry ist der **Docker Hub**, auf dem tausende öffentlich verfügbarer Images zur Verfügung stehen, aber auch "offizielle" Images.

Viele Organisationen und Firmen nutzen eigene Registries, um kommerzielle oder "private" Images zu hosten, aber auch um den Overhead zu vermeiden, der mit dem Herunterladen von Images über das Internet einhergeht. Die Nutzung eigener Registries (private Registries) kann auch aus Sicherheitsüberlegungen geschehen. Die Details entnehmen Sie der entsprechenden [Theorie](../Sicherheitsaspekte/Readme.md).

![docker archtecture](./../../Ressourcen/Images/docker%20architecture.svg)

Weiterführende Informationen zu [container registries](../container%20registry/Readme.md) erhalten Sie im Verlaufe des Moduls.

## docker command line interface (CLI)
In diesem [Dokument](CommandLineInterface.md) lernen Sie die gängigsten docker Befehle.

## Images erstellen mit dockerfile
In diesem [Dokument](Dockerfile.md) erklären wir Ihnen das Erstellen von Images anhand eines Files.

## Definition und Betrieb von Multicontainer mit docker-compose
In diesem [Dokument](DockerCompose.md) erklären wir Ihnen wie Sie einen Multicontainer betreiben.

---
**Quellen**

- https://docs.docker.com/get-started/overview/
- https://docs.docker.com/engine/reference/commandline/docker/





