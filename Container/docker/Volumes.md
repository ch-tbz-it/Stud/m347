# Arbeiten mit Volumes

[TOC]

## Lernziele
- Sie sind sich bewusst, dass Daten, die persistent gespeichert werden sollen, ausserhalb des Containers (in Volumes) abgelegt werden müssen.
- Sie kennen die drei Arten von Volumes bei Docker (bind mounts, volume und tmpfs)
- Sie kennen das Konzept der mounts in Linux
- Sie können Volumes bei der Entwicklung von Containern sinnvoll einsetzen

## Einstieg
Sie haben gelernt, dass die Daten einer Applikation (Daten in der Datenbank, Benutzer-Uploads, etc.) nicht im Container gespeichert werden dürfen, weil diese Daten nach Beenden des Containers vernichtet werden. Deshalb braucht es einen anderen Ort, um die Daten zu speichern, die auch nach Beenden des Containers noch verfügbar sein sollen.

### Linux und Mounts - Grundlage für alle Arten von Volumes
Mounten von Verzeichnissen in Linux bedeutet, dass Sie ein Dateiverzeichnis in einem anderen System oder unter einem anderen Pfad so einbinden, dass es dort erreichbar ist. Ein Beispiel:
- Gegeben ist der Pfad `C:\daten` im Host-System
- Gegeben ist der Pfad `/mnt/meinMountPoint` im Container
- Wenn Sie nun den Hostpfad `C:\daten` unter dem Pfad `/mnt/meinMountPoint` mounten und im Ordner `C:\daten` ein File `text.txt` ablegen, dann können Sie innerhalb des Containers über den Pfad `/mnt/meinMountPoint/text.txt` auf das File im Hostsystem zugreifen

Dank den Mounts haben Sie theoretisch auch die Möglichkeit nicht nur Filesysteme des Host-Rechners innerhalb des Containers zu mounten, sondern können auch Netzwerkfilesysteme (Clusterstorage, NFS - Network File System, etc.) innerhalb des Containers verfügbar zu machen.

### Docker
Im Zusammenhang mit Docker Container existieren folgende Möglichkeiten:
- bind mounts (auf der Festplatte des Hostrechners)
- volume
- tmpfs mounts (im Speicher vom Hostrechner)

![Quelle Bild: https://docs.docker.com/storage/volumes/](../../../Ressourcen/Images/types-of-mounts.png)

Quelle Bild: https://docs.docker.com/storage/volumes/

Dabei ist ein **bind mount** optimal, wenn Sie den Folder mit Ihrem Code mit dem Container sharen wollen. Dies ist vor allem bei der Entwicklung des Container-Inhalts (der eigentlichen Applikation) nützlich, weil so der Code verändert werden kann und dieser auch im Container danach verändert ist ohne das der Container jedes Mal neu gebuildet und gestartet werden muss. Weitere Details finden Sie in der [offiziellen Docker Dokumentation](https://docs.docker.com/storage/bind-mounts/).

Bei **volume** wird zwischen benannten (*named volumes*) und unbenannten volumes (*anonymous volumes*) unterschieden. Die Volumes mit Namen lassen sich einfacher wiederverwenden bzw. von anderen Containern aus einfach über deren Namen finden lassen. Die Volumes werden dabei von Docker verwaltet. Es wird auch durch Docker entschieden, wo die Daten abgelegt werden. Weitere Details finden Sie in der [offiziellen Docker Dokumentation](https://docs.docker.com/storage/volumes/).

Die Files die in einem **tmpfs mount** abgelegt werden, existieren nur so lange wie der Hostrechner eingeschalten ist, weil diese im Memory (RAM) des Hostrechners abgelegt werden. Diese Art von mount ist für Arbeitsdaten sinnvoll, die kurzfristig (nicht dauerhaft) abgelegt werden müssen. Weitere Details finden Sie in der [offiziellen Docker Dokumentation](https://docs.docker.com/storage/tmpfs/).

## Erstellen von Volumes
Es gibt verschiedene Arten, um die verschiedenen Volumes anzulegen, zu verwenden und Verwalten. Es gibt zudem Unterschiede ob CLI oder YAML verwendet wird.

### CLI
Volumes können mit folgenden Optionen angelegt werden:
- `--mount`: Die Option kann alles anlegen (**bind mounts**, **Volumes** und **tmpfs**) und wird von Docker zur Verwendung empfohlen
- `-v`: Mit dieser Option können nur **bind mounts** und **volumes** angelegt werden.
- `--tmpfs`: Mit dieser Option können nur **tmpfs** angelegt werden.

Folgendes Beispiel zeigt wie Sie ein Volume direkt mit dem `docker run`-Befehl anlegen können.
```bash
docker run -v myVolume:/mount/pfad/in/container -d nginx
```

Sie haben aber auch die Möglichkeit, mit dem Befehl `docker volume create` ein neues Volume anzulegen. Für weitere Details zu den Befehlen und den Möglichkeiten schauen Sie bitte in der [offiziellen Dokumentation](https://docs.docker.com/storage/) nach.

### YAML
Auch mit YAML können die verschiedenen Arten von Volumes erstellt werden. Die folgende YAML-Datei zeigt beispielhaft die Erstellung von zwei Volumes die in einem Container mit Name **web** eingehängt werden:

```YAML
version: "3.9"
services:
  web:
    image: nginx:alpine
    volumes:
      - type: volume
        source: mydata
        target: /data
        volume:
          nocopy: true
      - type: bind
        source: ./static
        target: /opt/app/static
```
[Quelle Code & weitere Details:](https://docs.docker.com/compose/compose-file/compose-file-v3/#volumes)

Beim ersten Volume handelt es sich um ein klassisches **volume** mit dem Namen **mydata**. Das zweite ist ein **bind mount**.
