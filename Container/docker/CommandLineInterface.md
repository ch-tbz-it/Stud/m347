# docker command line interface (CLI)

[TOC]

Der Docker Client bietet eine Vielzahl von Befehlen, die für die Bedienung der Anwendung genutzt werden können. In diesem Abschnitt wird eine Auswahl von Befehlen näher beleuchtet. 

Um mehr informationen zu den Befehlen und deren Verwendung zu erhalten, hat der Docker-Befehl Hilfeseiten, die man sich anzeigen lassen kann und die einem mehr Informationen über die Befehle geben. Wird beispielsweise nur `docker` als Befehl angegeben, listet das Programm auf, welche möglichen Befehle es gibt und das mit `docker COMMAND --help` weitere Hilfe-Seiten aufgerufen werden können für die einzelnen Befehle, die mehr ins Detail gehen für den spezifischen Befehl. So kann beispielsweise mit `docker run --help` die Hilfe-Seite von dem run-Befehl angezeigt werden.

Zusätzlich kann dieses [Video](https://www.youtube.com/watch?v=xGn7cFR3ARU) zur Erklärung der Commands beigezogen werden.

## docker run

- Ist der Befehl zum Starten neuer Container.
- Der bei weitem komplexesten Befehl, er unterstützt eine lange Liste möglicher Argumente.
- Ermöglicht es dem Anwender, zu konfigurieren, wie das Image laufen soll, Dockerfile-Einstellungen zu überschreiben, Verbindungen zu konfigurieren und Berechtigungen und Ressourcen für den Container zu setzen.


Standard-Test:

    $ docker run hello-world
Startet einen Container mit einer interaktiven Shell (interactive, tty):

    $ docker run -it ubuntu /bin/bash
Startet einen Container, der im Hintergrund (detach) läuft:

    $ docker run -d ubuntu sleep 20
Startet einen Container im Hintergrund und löscht (remove) diesen nach Beendigung des Jobs:

    $ docker run -d --rm ubuntu sleep 20
Startet einen Container im Hintergrund und legt eine Datei an:

    $ docker run -d ubuntu touch /tmp/lock
Startet einen Container im Hintergrund und gibt das ROOT-Verzeichnis (/) nach STDOUT aus:

    $ docker run -d ubuntu ls -l
docker ps

Gibt einen Überblick über die aktuellen Container, wie z.B. Namen, IDs und Status.
Aktive Container anzeigen:

    $ docker ps
Aktive und beendete Container anzeigen (all):

    $ docker ps -a
Nur IDs ausgeben (all, quit):

    $ docker ps -a -q
docker images

- Gibt eine Liste lokaler Images aus, wobei Informationen zu Repository-Namen, Tag-Namen und Grösse enthalten sind.


Lokale Images ausgeben:

    $ docker images
Alternativ auch mit ... image ls:

    $ docker image ls
docker rm und docker rmi

docker rm
- Entfernt einen oder mehrere Container. Gibt die Namen oder IDs erfolgreich gelöschter Container zurück.
docker rmi
- Löscht das oder die angegebenen Images. Diese werden durch ihre ID oder Repository- und Tag-Namen spezifiziert.


Docker Container löschen:

    $ docker rm [name]
Alle beendeten Container löschen:

    $ docker rm $(docker ps --filter status=exited -q)
Alle Container, auch aktive, löschen:

    $ docker rm -f $(docker ps -a -q)
Docker Image löschen:

    $ docker rmi ubuntu
Zwischenimages löschen (haben keinen Namen):

    $ docker rmi `docker images -q -f dangling=true`

docker start

- Startet einen (oder mehrere) gestoppte Container.
    - Kann genutzt werden, um einen Container neu zu starten, der beendet wurde, oder um einen Container zu starten, der mit docker create erzeugt, aber nie gestartet wurde.


Docker Container neu starten, die Daten bleiben erhalten:

    $ docker start [id]

## Container stoppen, killen

docker stop
- Stoppt einen oder mehrere Container (ohne sie zu entfernen). Nach dem Aufruf von docker stop für einen Container wird er in den Status »exited« überführt.

docker kill
- Schickt ein Signal an den Hauptprozess (PID 1) in einem Container. Standardmässig wird SIGKILL gesendet, womit der Container sofort stoppt.

## Informationen zu Containern

docker logs
- Gibt die "Logs" für einen Container aus. Dabei handelt es sich einfach um alles, was innerhalb des Containers nach STDERR oder STDOUT geschrieben wurde.

docker inspect
- Gibt umfangreiche Informationen zu Containern oder Images aus. Dazu gehören die meisten Konfigurationsoptionen und Netzwerkeinstellungen sowie Volumes-Mappings.

- docker diff
Gibt die Änderungen am Dateisystem des Containers verglichen mit dem Image aus, aus dem er gestartet wurde.

docker top
- Gibt Informationen zu den laufenden Prozessen in einem angegebenen Container aus.

