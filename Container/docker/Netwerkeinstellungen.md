# Docker Netzwerkeinstellungen

[TOC]

## Überblick
Bei docker gibt es viele unterschiedliche Arten von Netzwerken bzw. wie die Netzwerke konfiguriert werden können. Die unterschiedlichen Arten von Netzwerken unterscheiden sich im Wesentlichen dadurch, dass die Container bzw. die Netzwerkkommunikation der Container unterschiedlich stark voneinander isoliert werden können. Der Film <https://www.youtube.com/watch?v=bKFMS5C4CG0> erklärt die unterschiedlichen verfügbaren Netzwerkarten. 

Im Unterricht befassen wir uns nur mit dem **bridge network** (<https://docs.docker.com/network/bridge/>). Dabei unterscheiden wir zwei Arten:
* default bridge
* user-defined bridge

Die beiden Bridge-Netzwerke funktionieren von der Art her sehr ähnlich. Den Containern in diesem Netz werden automatisch IP-Adressen via DHCP zugewiesen. Durch einen "virtuellen Router" werden die Container mit dem Netzwerk des Hostrechners verbunden.

## user-defined bridge im Detail
Die Entwickler von Docker empfehlen die **user-defined bridge** zu verwenden. Diese bietet weitere Möglichkeiten, die so durch die **default bridge** nicht unterstützt werden.

### Unterschiede zur default bridge
Unterschiede von der user-defined bridge zur default bridge sind die folgenden:
* **Namensauflösung der Containernamen**: Heisst Ihr Container beispielsweise **my_funny_name** dann können Sie beispielsweise über `ping my_funny_name` den Container von einem anderen Container im selben Netzwerk aus anpingen, ohne dessen IP-Adresse kennen zu müssen.
* **Isolation von Containern**: Dank dem Umstand, dass Sie beinahe beliebig viele **user-defined bridge** Netzwerke anlegen können, können Sie die Container, die miteinander kommunizieren können müssen, in dasselbe Netzwerk packen. Andere Container in anderen **user-defined bridge** Netzwerken können dann auf diese Container ausserhalb Ihres Netzwerkes nicht ohne weiteres zugreifen.
* **Generell flexibler**: Netzwerkkonfiguration - sowohl der user-defined bridge wie auch der Container selbst - können während dem Betrieb verändert werden. Bei der **default bridge** ist es nötig Docker bei Anpassungen neu zu starten.

### Docker Network Befehle
Ein user-defined bridge network lässt sich via `docker network create` anlegen. Beachten Sie, dass Sie das Netzwerk erst anlegen müssen, bevor Sie dieses einem Container zuweisen können.

Soll beispielsweise das Netzwerk mit Name `my-network` angelegt werden, geschieht dies mit dem Befehl `docker network create my-network`. Docker bestimmt dabei selbst wie gross das Netzwerk sein wird und was der dem Netzwerk zugewiesene IP-Range ist. Mit `docker network ls` können die angelegten Netzwerke angezeigt werden. Mit `docker inspect my-network` kann angeschaut werden, welchen Netzwerkrange Docker für das Netzwerk mit dem Namen **my-network** gewählt hat. Mit `docker network rm my-network` kann das Netzwerk schliesslich auch wieder gelöscht werden.

### Zuweisung von Containern in ein user-defined bridge network
Mit dem `--network` flag kann angegeben werden, welches Netzwerk dem Container zugewiesen werden soll.

So wird beispielsweise mit 
```bash
docker create --name my-nginx --network my-network nginx:latest
```
... ein neuer Container mit Name my-nginx vom Image nginx in der Version latest angelegt und ein user-defined bridge Netzwerk mit Namen my-network dem Container angehängt. Ist das Netzwerk my-network noch nicht vorhanden, wird dieses durch Docker automatisch erstellt (Netzwerk wird implizit erstellt).

Sollte es nötig sein mehrere Netzwerke an einen Container zu hängen, kann mit `docker network connect my-network-2 my_container` dem Container mit Namen **my_container** das Netzwerk **my-network-2** nach dem Start zugewiesen werden.
