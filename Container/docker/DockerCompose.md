# docker-compose: Multicontainer definieren und betreiben

Docker Compose ist ein Tool, das die Verwendung von mehreren Docker-Containern als Teil einer Anwendung erleichtert. Es ermöglicht die einfache Konfiguration und Verwaltung von Docker-Containern und deren Interaktion miteinander.

Die Docker-Compose-Datei ist eine YAML-Datei, die verwendet wird, um die Konfiguration der Anwendung und die Docker-Container zu definieren. Sie besteht aus einer Version, Services und Netzwerken, die in YAML-Format angegeben sind.

Die Version gibt an, welche Version der Docker-Compose-Datei verwendet wird, wie zum Beispiel "version: '3.9'".

Die Services definieren die verschiedenen Container, die Teil der Anwendung sind. Für jeden Service gibt es eine Liste von Parametern, wie zum Beispiel der Name des Containers, das verwendete Docker-Image, die Portweiterleitung, die Volumes und Umgebungsvariablen.

Hier ist ein Beispiel für die Services-Struktur in einer Docker-Compose-Datei:

```yaml
version: '3.9'
name: web_app
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
  redis:
    image: "redis:alpine"
```
In diesem Beispiel gibt es zwei Services: "web" und "redis". Der "web"-Service wird erstellt, indem das Docker-Image aus dem Build-Context im aktuellen Verzeichnis gebaut wird. Der Container veröffentlicht den Port 5000 und bindet das lokale Verzeichnis ".:/code" an das Verzeichnis "/code" im Container an. Der "redis"-Service verwendet ein vorhandenes Redis-Image, das aus Docker Hub geladen wird.

Die Netzwerke in der Docker-Compose-Datei ermöglichen es, Netzwerke zwischen den Containern zu erstellen, so dass sie miteinander kommunizieren können. Netzwerke können für alle Services oder für bestimmte Services spezifiziert werden.

Um eine Docker Compose-Datei auszuführen, navigieren Sie zu dem Verzeichnis, das die YAML-Datei enthält, und führen Sie den folgenden Befehl aus. Die Datei sollte in diesem Fall ***compose.yaml*** benannt sein

```bash
# Befehl, falls die Datei compose.yaml heisst
docker-compose up
# alternativer Befehl für beliebige Namen/Pfade
docker-compose -f ihre-datei.yaml up
```
Dies wird die Anwendung starten und die Container gemäß den in der YAML-Datei definierten Spezifikationen erstellen. Sie können auch spezifische Container starten oder stoppen oder Container-Skalierung und andere Aktionen mit Docker-Compose durchführen.

Insgesamt bietet Docker-Compose eine einfache Möglichkeit, mehrere Container in einer Anwendung zu definieren und auszuführen, was die Verwaltung und Bereitstellung von Anwendungen erheblich erleichtert. Die YAML-Datei ist einfach zu schreiben und zu verstehen und ermöglicht es, Container und deren Interaktion auf eine übersichtliche und effektive Weise zu definieren.

---
**Quellen**:

- Einstieg: https://docs.docker.com/compose/

- Referenz zur YAML-Datei: <https://docs.docker.com/compose/compose-file/>