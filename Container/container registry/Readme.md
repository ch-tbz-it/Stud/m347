# container registry

[TOC]

Ein Container-Registry ist ein zentraler Speicherplatz für Docker-Images oder andere Container-Images. Docker-Images oder Container-Images sind Vorlagen, die genutzt werden, um Container zu erstellen. Container-Images enthalten alle notwendigen Abhängigkeiten und Konfigurationsinformationen, um eine Anwendung oder einen Dienst innerhalb eines Containers zu betreiben.

Ein Container-Registry ermöglicht es Benutzern, Docker-Images oder andere Container-Images zu verwalten und zu speichern. Benutzer können diese Images einfach herunterladen (*pull*) und auf ihren Systemen ausführen, um Container zu erstellen und Anwendungen innerhalb dieser Container auszuführen. Images lassen sich auch hochladen (*push*), um für andere Benutzer diese für den Download zur Verfügung zu stellen. Container-Registrys können [öffentlich (*public*)](publicContainerRegistry.md) sein, was bedeutet, dass jeder auf sie zugreifen und Images herunterladen kann. Es gibt auch [private Registry (*private*)](privateContainerRegistry.md), was bedeutet, dass sie nur für autorisierte Benutzer oder Organisationen zugänglich sind.

Die Verwendung eines Container-Registrys hat viele Vorteile, darunter die Möglichkeit, Images zentral zu speichern und zu verwalten, die Wiederverwendbarkeit von Images zu fördern, die Reproduzierbarkeit von Builds (Herstellung von Versions-Zuständen) zu gewährleisten und die Verteilung von Images über verschiedene Systeme und Umgebungen zu erleichtern. Container-Registrys sind ein wichtiger Bestandteil von DevOps und der modernen Anwendungsentwicklung und ermöglichen es Unternehmen, schnellere und effizientere Software-Entwicklungs- und Bereitstellungsprozesse zu implementieren.

![](../../Ressourcen/Images/containerRegistry.png)

## Versionierung von Images in container registries

Die Versionierung von Images in einem Container-Registry wird oft mit sogenannten Tags durchgeführt. Tags sind einfach zu verwaltende Kennzeichnungen, die einem Container-Image zugewiesen werden, um es innerhalb des Container-Registries eindeutig zu identifizieren und zu versionieren.

Ein Tag ist im Grunde eine Zeichenfolge, die dem Namen des Images angehängt wird und es kennzeichnet. Ein Tag kann aus einer Version, einem Datum, einem Namen oder einer anderen Kennzeichnung bestehen, die der Entwickler oder Administrator wählt. Container-Registries ermöglichen es Ihnen, verschiedene Tags auf dasselbe Image anzuwenden, was es Benutzern ermöglicht, verschiedene Versionen des Images mit unterschiedlichen Tags abzurufen.

Ein Beispiel für die Verwendung von Tags in einem Container-Registry wäre die Kennzeichnung eines Images mit der Versionsnummer "1.0.0" und dem Tag "latest". Wenn Sie später ein Update an der Anwendung durchführen und ein neues Image erstellen, können Sie es mit derselben Versionsnummer "1.0.0" versehen, aber mit einem neuen Tag, z.B. "myapp:1.0.0-update1". Dadurch können Benutzer und Entwickler zwischen verschiedenen Versionen des Images wählen, je nachdem, welche Tags ihnen zur Verfügung stehen.

Ein weiterer nützlicher Aspekt der Verwendung von Tags in einem Container-Registry ist, dass sie dazu beitragen können, die Kompatibilität zwischen verschiedenen Anwendungen und Diensten sicherzustellen. Wenn ein Image mit einem bestimmten Tag ausgeführt wird, kann man sicher sein, dass es mit anderen Anwendungen und Diensten, die ebenfalls mit diesem Tag arbeiten, kompatibel ist.

![](../../Ressourcen/Images/containerRegistryTag.png)

