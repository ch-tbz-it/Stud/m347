# dockerhub Anleitung

Dockerhub ist eine Versionierungssoftware für container images.

Eine Repository entspricht eine Versionierung für eine image.

## step by step Anleitung: image versionieren

1. docker repository in docker hub erstellen (free plan nur 1 private repo möglich)
2. lokal: image erstellen/ builden
3. lokal: einloggen mit `<username>` und `<passwort>` von dockerhub
   ```bash
   docker login 
   ```
4. lokal: image tag setzen. Z.B. mein vorher erstelltes image

   ```bash
   docker images
   ```

   Aus der obigen Liste das Image wählen und nachfolgend setzen:

   ```bash
   docker image tag myimage:latest <username>/mytest
   ```
   oder
   ```bash
   docker tag myimage:latest <username>/mytest:latest
   ```
5. lokal: image pushen
   ```bash
   docker image push <username>/mytest:latest
   ```
   oder
   ```bash
   docker push <username>/mytest:latest
   ```

---
Quellen:
- <https://docs.docker.com/engine/reference/commandline/push/#push-a-new-image-to-a-registry>
- <https://docs.docker.com/registry/>
- <https://coderefinery.github.io/reproducible-research/docker/>
