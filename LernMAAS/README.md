## Wieso LernMAAS?
LernMAAS ist ein System, dass von Marcel Bernet aus alten ausrangierten Rechnern der TBZ aufgebaut wurde. Es basiert zu 100% auf OpenSource-Software und macht aus den alten ausrangierten Computer der TBZ ein hochverfügbaren Cluster - ähnlich wie dies von AWS oder Azure bekannt ist. Auf diesem Cluster können automatisiert VM's deployed werden.

LernMAAS ist dazu da Ihnen für die verschiedenen Informatik-Module VM's zur Verfügung zu stellen, wo bereits die für das Modul nötige Software vorinstalliert ist. Es ist eine Möglichkeit "Laborbedingungen" für die Hands-on Aufgaben in diesem Modul zu schaffen. Sollte Ihr Rechner nicht genügend leistungsfähig sein, oder möchten Sie gerne auf einer Linux-VM anstatt auf Ihrem Windows oder OS X System arbeiten, dann bietet das LernMAAS für jeden Lernenden eine eigene VM. In unserem Fall ist die VM ein Ubuntu-Server auf dem sowohl docker wie auch microk8s vorinstalliert ist und für die Übungen / Hands-On anstatt Ihrer eigenen lokalen Umgebung eingesetzt werden kann.

**Wichtig:** Wenn das Modul beendet ist, wird Ihre VM ohne Backup wieder entsorgt. D. h. Daten, die Sie nach Modulende noch brauchen, müssen Sie vorher selbstständig bei sich lokal sichern.

### Zugriff auf Ihre VM
Wie Sie den Zugriff auf Ihre VM erlangen finden Sie im Detail unter [ZugriffVM.md](Hands-on/ZugriffVM/ZugriffVM.md) beschrieben.