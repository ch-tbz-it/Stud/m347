# LernMAAS
[TOC]

## Zugriff auf LernMAAS
Der Zugriff auf das LernMAAS geschieht via VPN-Verbindung. Durch den Aufbau eines VPN-Tunnels via WireGuard können Sie Ihren Rechner quasi im gleichen Netzwerk anschliessen wie auch die VM's des LernMAAS zur Verfügung stehen.

Weil Sie Ihren Rechner im LernMAAS-Netzwerk anschliessen, braucht Ihr Rechner auch eine IP aus dem entsprechenden Netzwerk. Damit es keine IP-Konflikte (zwei gleiche IP's im Netzwerk) gibt, braucht die Vergabe der IP-Adressen etwas Organisation. Die Lehrperson informiert Sie darüber,
- welche IP-Adresse Sie für Ihren lokalen Rechner verwenden dürfen.
- welchen Key Sie für die Authentifizierung im VPN verwenden müssen.
- welche IP-Adresse Ihre persönliche VM besitzt.

Diese Informationen benötigen Sie, um den VPN-Tunnel einzurichten und auf die VM Zugriff zu erhalten.

## Werden Sie zum "root" über Ihren Server (Ihre VM)
Um die "Herrschaft" über Ihren Virtuellen Server zu erlangen, gehen Sie bitte wie folgt vor:
1. Einrichten VPN-Tunnel und verbinden
2. Zugriff auf VM via HTTP -> Auslesen der SSH-Zugangsdaten, diese finden Sie im Tab "Accessing" zusammen mit einer Anleitung, wie Sie via SSH auf den Server verbinden
3. Zugriff auf die VM via SSH -> anschliessen Passwort des ubuntu-Benutzers ändern

Voraussetzung für den Zugriff auf Ihren Server ist, dass Sie WireGuard auf Ihrem Rechner installiert haben. Bei WireGuard handelt es sich um eine OpenSource-Software. Sollte die Software noch nicht vorhanden sein, laden Sie sich die Software unter https://www.wireguard.com/install/ bitte herunter und folgen Sie der Installationsanweisung, die der Installer Ihnen gibt.

### 1. Einrichten VPN-Tunnel und verbinden
Ein VPN-Tunnel bei WireGuard kann manuell oder via Import einer conf-Datei angelegt werden. Wir gehen den Weg über den Import einer conf-Datei. Dafür wird Ihnen die Lehrperson eine entsprechende Vorlage zustellen. In dieser Vorlage müssen Sie den Wert `<Replace IP>` mit Ihrer von der Lehrperson zugewiesenen IP-Adresse ersetzen. Den Wert `<Replace Key>` müssen Sie mit dem Ihnen von der Lehrperson zugewiesenen Key ersetzen.

Ist das gemacht, speichern Sie die conf-Datei ab und wählen Sie diese beim Import in WireGuard aus:

![Screenshot WireGuard import Tunnel](WireGuard_ScreenshotTunnelImport.png)

Der importierte Tunnel erhält den Namen der conf-Datei. Tipp benennen Sie diese vorher allenfalls etwas klarer, damit Sie danach noch wissen für das dieser VPN-Tunnel gedacht ist.

![Screenshot WireGuard nach abgeschlossenem Import](WireGuard_ScreenshotTunnelListe.png)

Mit einem Klick auf "Aktivieren" können Sie den VPN-Tunnel aufbauen und sich damit ins LernMAAS-Netzwerk einklinken.

### 2. Zugriff auf VM via HTTP
Öffnen Sie den Webbrowser Ihrer wahl und geben Sie http://`IP-Adresse Ihrer VM` ein (beispielsweise http://10.3.32.2). Die IP-Adresse Ihrer VM wird Ihnen durch die Lehrperson mitgeteilt.

**Wichtig:** Beachten Sie das Ihr VPN-Tunnel aus Schritt 1 aktiviert und richtig eingerichtet sein muss, damit die URL lädt.

Wenn alles richtig eingerichtet ist (und Sie die richtige URL verwendet haben), sollten Sie die folgende Oberfläche sehen:

![Screenshot VM Weboberfläche Accessing](VM_ScreenshotAccessing.png)

### 3. Zugriff auf die VM via SSH
Loggen Sie sich gemäss der Beschreibung auf der Weboberfläche Ihres Webservers in den Server via SSH als Benutzer ubuntu ein. Setzen Sie als erstes ein neues Passwort für den Ubuntu-Benutzer. Dies tun Sie mit folgendem Befehl:
`passwd`

Folgen Sie den Anweisungen in der command line. Nachdem das Passwort geändert ist, bauen Sie eine zweite SSH-Session auf und testen, ob Sie sich nun mit dem neuen Passwort einloggen können. Wenn dieser Test erfolgreich verlaufen ist, gehen Sie bitte ins Verzeichnis `/data/.ssh/` und legen Sie dort ein neues File an mit `touch pwchanged.txt` als "Nachweis", dass Sie erfolgreich auf den Server zugreifen und das Passwort ändern konnten.

**Wichtig:** Speichern Sie sich das neu gesetzte Passwort an einem sicheren Ort ab (beispielsweise in einem Passwort-Safe wie [KeePass](https://keepass.info/)).
