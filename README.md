![tbz_logo](Ressourcen/Images/tbz_logo.png)

[TOC]

---

# m347 - Dienst mit Container anwenden
Liebe Lernende

Willkommen im Modul 347 - Dienst mit Container anwenden. Modulidentifikation als [PDF](Ressourcen/Modulidentifikation_M347_Dienst_mit_Container_anwenden.pdf) oder [online](https://www.modulbaukasten.ch/module/347).

## Inhalt

[Docker & Docker Compose](./Container/)

[Kubernetes](./Kubernetes/)

## Kompetenznachweise

Sie werden sich die Inhalte ähnlich wie im Modul m346 selbstständig erarbeiten unter Anleitung der Lehrperson und den bereitgestellten Inhalten und Ressourcen

![Kompetenzen](./Ressourcen/Images/Kompetenzen.png)

## Lektionenplan

Sofern die Lehrperson nichts anderes kommuniziert, gilt der folgende Lektionenplan

| Tag    | Inhalt                                                       |
| ------ | ------------------------------------------------------------ |
| Tag 01 | Einführung in das Modul<br />Selbstständige Arbeit an Kompetenzen |
| Tag 02 | Selbstständige Arbeit an Kompetenzen<br />Input TBD          |
| Tag 03 | Selbstständige Arbeit an Kompetenzen<br />Input TBD          |
| Tag 04 | Selbstständige Arbeit an Kompetenzen<br />Input TBD          |
| Tag 05 | Selbstständige Arbeit an Kompetenzen<br />Input TBD          |
| Tag 06 | Selbstständige Arbeit an Kompetenzen<br />Input: Einführung in Kubernetes |
| Tag 07 | Selbstständige Arbeit an Kompetenzen<br />Input TBD          |
| Tag 08 | Selbstständige Arbeit an Kompetenzen                         |
| Tag 09 | KN 00 (Schriftliche Prüfung)<br />Selbstständige Arbeit an Kompetenzen |
| Tag 10 | Selbstständige Arbeit an Kompetenzen                         |

